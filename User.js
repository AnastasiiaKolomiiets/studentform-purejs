'use strict';
function User (name, phone, mail, year) {
    var properties = {
            _name: name,
            _phone: phone,
            _mail: mail,
            _year: year,
        };

    this.get = function (property) {
        return properties[property];
    };

    this.getAge = function () {
        var birhtDate = new Date(properties['_year']);
        return new Date().getFullYear() - birhtDate.getFullYear();
    };

    this.toJSON = function () {
        return {
            name: this.get('_name'),
            phone: this.get('_phone'),
            mail: this.get('_mail'),
            age: this.getAge
        };
    }

    return this;
}
