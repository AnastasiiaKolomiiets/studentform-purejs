'use strict';

function EditView (view) {
    var input = ['name', 'phone', 'mail', 'year'],
        button;

    createDOM();

    button = document.getElementById('toShowDataBtn');
    button.addEventListener('click', collectData, false);

    function createDOM () {
        var tpl = '<p>Your <=% name => :</p> <input type="text" placeholder="Your  <=% name => is here." class="userProperties">',
			collectTpl = '<div>';

        input.forEach(function (value) {
            var tplHandler = tpl;

			tplHandler = tplHandler.replace(/<=% name =>/g, value);
            collectTpl += tplHandler;
        });

        collectTpl += '<p><input type="button" value="Preview" id="toShowDataBtn"><p></div>';
        view.innerHTML = collectTpl;
    }

    function collectData () {
        var userInformation = {},
            userProperties = view.getElementsByClassName('userProperties'),
            user, showView;

        input.forEach(function (item, index) {
            userInformation[item] = userProperties[index].value;
        });

        button.removeEventListener('click', collectData, false);

        user = new User(userInformation['name'], userInformation['phone'], userInformation['mail'],         userInformation['year']);
        showView = new ShowView(user, view);
    }

    return this;
}
