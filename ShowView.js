'use strict';

function ShowView (user, view) {

    showData(user.toJSON());

    var button = document.getElementById('toEditDataBtn');
    button.addEventListener('click', change, false);

    function showData (user) {
        //var tpl = '<p>Your <=% property => :</p> <input type="text" placeholder= <=% value => >',
         var tpl = '<p>Your <=% property => :</p> <div>  <=% value => </div>',
        //var tpl = '<div><span>your </span><span><=% property =>: </span><span><=% value =></span></div>',
            btnTpl = '<p><input type="button" value="Edit" id="toEditDataBtn"></p>',
            show = '', tplHandler;

        for (var key in user) {
            tplHandler = tpl;
            tplHandler = tplHandler.replace('<=% property =>', key);
            tplHandler = tplHandler.replace('<=% value =>', user[key]);
            show += tplHandler;
        }
        view.innerHTML = show + btnTpl;
    }

    function change () {
        button.removeEventListener('click', change, false);
        var edit = new EditView(view);

    }

    return this;
}
